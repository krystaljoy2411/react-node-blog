React Blog

TASKS

1] send username along with token from backend. Use this username to show on the homepage nav bar. Create a Navbar component and show username, your company logo and a logout button in it. 

2 ] Create a Signup page like the Login Page. Once Signed up/ or logged In, the user should be redirected to the Home Page. 

3] Create a logout button in the navbar. Clicking on which the token and username in localstorage is deleted and the user is redirected to the login page. 

4] create a ViewArticle component. It takes in props as “Title”, “text”, “author”.
Displays this in a box-like component. 

5] In the home page, replicate the ViewArticle component for a number of articles from the backend.

====================================Optionals=====================

6] If the author for that article is the same as the userKey stored in local storage, show edit and delete buttons. Create components and edit functionality for editing a blog. Use this same component for create blog feature. 

7] Anything creative, Error handling in front-end in case the backend is not working. 

==================================================================
GUIDELINES

1] Have meaningful variable/ method names.
2] Follow the project structure.
